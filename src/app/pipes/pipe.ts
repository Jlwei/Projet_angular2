import { Pipe, PipeTransform } from '@angular/core';

// Tell Angular2 we're creating a Pipe with TypeScript decorators
@Pipe({
  name: 'SeancePipe'
})
export class SeancePipe implements PipeTransform {

  // Transform is the new "return function(value, args)" in Angular 1.x
  transform(value, ...args) {
    let cinema = args[0];
    let film = args[1];
    let date = args[2];
    
    // ES6 array destructuring
    if (cinema == undefined || cinema == ''){
        if (film == undefined || film == ''){
            console.log(date);
            if(date == undefined || date == ''){
                return value.filter(seance => { return true;});
            }
            else{
                return value.filter(seance => { return seance.date.getTime() >= (new Date(date)).getTime();});
            }
        }
        else{
            if(date == undefined || date == ''){
                return value.filter(seance => { return seance.film == film; });
            }
            else{
                return value.filter(seance => { return seance.film == film && seance.date.getTime() >= (new Date(date)).getTime(); });
            }
        }
    }
    else{
        if (film == undefined || film == ''){
            if(date == undefined || date == ''){
                return value.filter(seance => { return seance.cinema == cinema; });
            }
            else{
                return value.filter(seance => { return seance.cinema == cinema && seance.date.getTime() >= (new Date(date)).getTime(); });
            }
        }
        else{
            if(date == undefined || date == ''){
                return value.filter(seance => { return seance.cinema == cinema && seance.film == film; });
            }
            else{
                return value.filter(seance => { return seance.cinema == cinema && seance.film == film && seance.date.getTime() >= (new Date(date)).getTime(); });
            }
        }
    }
    
  }
}
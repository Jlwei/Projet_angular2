import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms'; // <-- NgModel lives here


import { AppComponent } from './app.component';
import { SeanceComponent } from './seance/seance.component';
import { SeancePipe } from 'app/pipes/pipe';

@NgModule({
  declarations: [
    AppComponent,
    SeanceComponent,
    SeancePipe,
  ],
  imports: [
    BrowserModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

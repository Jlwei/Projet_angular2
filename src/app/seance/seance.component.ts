import { Component, OnInit, Input } from '@angular/core';
import { Seance } from 'app/seance/seance.model';

@Component({
  selector: 'app-seance',
  templateUrl: './seance.component.html',
  styleUrls: ['./seance.component.css'],
  host: {
    class: 'row'
  }
})
export class SeanceComponent implements OnInit {
  
  @Input() seance: Seance;
  
  ngOnInit() {
  }

}

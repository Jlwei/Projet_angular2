export class Seance {
    picture: string;
    film: string;
    date_de_sortie: string;
    realisateur: string;
    acteur: string;
    genre: string;
    introduction: string;
    cinema: string;
    salle: string;
    date: Date;
    
    constructor(
            picture: string,
            film: string,
            date_de_sortie: string,
            realisateur: string,
            acteur: string,
            genre: string,
            introduction: string,
            cinema: string,
            salle: string,
            date: Date) 
    {
        this.picture = picture;
        this.film = film;
        this.date_de_sortie = date_de_sortie;
        this.realisateur = realisateur;
        this.acteur = acteur;
        this.genre = genre;
        this.introduction = introduction;
        this.cinema = cinema;
        this.salle = salle;
        this.date = date;
    }
}
import { Component } from '@angular/core';
import { Seance } from 'app/seance/seance.model';
import { SeancePipe } from 'app/pipes/pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  seances : Seance[];
  
  constructor() {
    this.seances = [
      new Seance( '/assets/images/Your Name.jpg','Your Name','28 décembre 2016 (1h46min)','Makoto Shinkai','Ryûnosuke Kamiki, Mone Kamishiraishi','Animation , Drame','Mitsuha, adolescente coincée dans une famille traditionnelle, rêve de quitter ses montagnes natales pour découvrir la vie trépidante de Tokyo. Elle est loin d’imaginer pouvoir vivre l’aventure urbaine dans la peau de… Taki, un jeune lycéen vivant à Tokyo, occupé entre son petit boulot dans un restaurant italien et ses nombreux amis.','Pathé Belfort','A',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Belfort','A',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Belfort','A',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Belfort','A',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Belfort','A',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Belfort','A',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Beaux-Arts','A',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Beaux-Arts','A',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Beaux-Arts','A',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Beaux-Arts','A',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Beaux-Arts','A',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/Your Name.jpg','Your Name','','','','','','Pathé Beaux-Arts','A',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Belfort','B',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Belfort','B',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Belfort','B',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Belfort','B',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Belfort','B',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Belfort','B',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Beaux-Arts','B',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Beaux-Arts','B',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Beaux-Arts','B',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Beaux-Arts','B',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Beaux-Arts','B',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/Forrest Gump.jpg','Forrest Gump','','','','','','Pathé Beaux-Arts','B',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Belfort','C',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Belfort','C',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Belfort','C',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Belfort','C',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Belfort','C',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Belfort','C',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Beaux-Arts','C',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Beaux-Arts','C',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Beaux-Arts','C',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Beaux-Arts','C',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Beaux-Arts','C',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/Le Parrain.jpg','Le Parrain','','','','','','Pathé Beaux-Arts','C',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Belfort','D',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Belfort','D',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Belfort','D',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Belfort','D',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Belfort','D',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Belfort','D',new Date(2017, 5,15, 16,0o0,0)),
      
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Beaux-Arts','D',new Date(2017, 5,14, 11,0o0,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Beaux-Arts','D',new Date(2017, 5,14, 14,30,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Beaux-Arts','D',new Date(2017, 5,14, 16,0o0,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Beaux-Arts','D',new Date(2017, 5,15, 11,0o0,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Beaux-Arts','D',new Date(2017, 5,15, 14,30,0)),
      new Seance( '/assets/images/The Dark Knight, Le Chevalier Noir.jpg','The Dark Knight, Le Chevalier Noir','','','','','','Pathé Beaux-Arts','D',new Date(2017, 5,15, 16,0o0,0)),
    ];
  }
  
  sortedSeances(): Seance[] {
    return this.seances.sort((a: Seance, b: Seance) => ( a.date > b.date ? 1:-1 ));
  }
  
  rechercherSeance(cinema: HTMLSelectElement, film: HTMLSelectElement): boolean {
    console.log(`Film: ${film.value} and Cinema: ${cinema.value}`);
    return false;
  }
  
}
